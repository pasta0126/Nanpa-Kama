﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nanpa_Kama.Core
{
    public static class Valores
    {
        public static readonly Modo Mode = Modo.Produccion;

        public enum Modo
        {
            Desarrollo = 0,
            Integracion = 2,
            Validacion = 3,
            Produccion = 1
        }

        public enum LogConceptos
        {
            Generar = 10,
            Agregar = 11,
            Validar = 20,
            ValidarLista = 21,
            ValidarHistorico = 22,
            SinValidacion = 29,
            Filtros = 30,
            FiltrolaParImpar = 31,
            FiltroSeguidos = 32,
            Descartado = 90,
            DescartadoLista = 91,
            DescartadoHistorico = 92,
            DescartadoFiltros = 93,
            DescartadoFiltroParImpar = 94,
            DescartadoFiltroSeguidos = 95
        }
    }
}

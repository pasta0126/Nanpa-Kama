﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nanpa_Kama.Core.Combinaciones
{
    public class Acciones
    {
        public Acciones()
        {

        }

        public Model.t_combinaciones Convert(Model.Data.Combinacion comb)
        {
            Model.t_combinaciones c = new Model.t_combinaciones();

            c.d1 = comb.d1;
            c.d2 = comb.d2;
            c.d3 = comb.d3;
            c.d4 = comb.d4;
            c.d5 = comb.d5;
            c.d6 = comb.d6;
            c.complementario = comb.complementario;
            c.joker = comb.joker;
            c.comentario = string.Empty;
            c.fecha_generado = DateTime.Today;

            return c;
        }
    }
}

﻿namespace Nanpa_Kama.Core.Combinaciones
{
    public static class Valores
    {
        public static readonly int Campos = 6;

        public enum Condiciones
        {
            Coincidencias3 = 3,
            Coincidencias4 = 4,
            Coincidencias5 = 5,
            Coincidencias6 = 6
        }
    }
}

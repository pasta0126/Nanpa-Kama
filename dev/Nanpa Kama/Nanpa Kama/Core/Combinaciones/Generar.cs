﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

namespace Nanpa_Kama.Core.Combinaciones
{
    public class Generar
    {
        public readonly Model.Data.Combinacion combinacion = new Model.Data.Combinacion();
        public readonly string ElapsedTime = string.Empty;

        public Generar()
        {
            Stopwatch stopWatch = new Stopwatch();

            stopWatch.Start();

            combinacion = GenerarCombinacion();

            stopWatch.Stop();

            TimeSpan time = stopWatch.Elapsed;
            ElapsedTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00}", time.Hours, time.Minutes, time.Seconds, time.Milliseconds / 10);
        }

        private Model.Data.Combinacion GenerarCombinacion()
        {
            Model.Data.Combinacion comb = new Model.Data.Combinacion();

            for (int i = 1; i < Valores.Campos + 1; i++)
            {
                int valor = GenerarValor();

                while (valor == comb.d1 || valor == comb.d2 || valor == comb.d3 || valor == comb.d4 || valor == comb.d5 || valor == comb.d6)
                {
                    valor = GenerarValor();
                }

                switch (i)
                {
                    case 1:
                        comb.d1 = valor;
                        break;

                    case 2:
                        comb.d2 = valor;
                        break;

                    case 3:
                        comb.d3 = valor;
                        break;

                    case 4:
                        comb.d4 = valor;
                        break;

                    case 5:
                        comb.d5 = valor;
                        break;

                    case 6:
                        comb.d6 = valor;
                        break;

                    default:
                        break;
                }
            }

            OrdenarCombinacion(comb);

            return comb;
        }

        private int GenerarValor()
        {
            Random r = new Random(DateTime.Now.Millisecond);
            int num = r.Next(1, 50);

            while (num >= 50)
            {
                num = r.Next(1, 50);
            }

            return num;
        }

        private void OrdenarCombinacion(Model.Data.Combinacion comb)
        {
            List<int> campos = new List<int>
            {
                comb.d1,
                comb.d2,
                comb.d3,
                comb.d4,
                comb.d5,
                comb.d6
            };

            campos.Sort();

            comb.d1 = campos[0];
            comb.d2 = campos[1];
            comb.d3 = campos[2];
            comb.d4 = campos[3];
            comb.d5 = campos[4];
            comb.d6 = campos[5];
        }
    }
}

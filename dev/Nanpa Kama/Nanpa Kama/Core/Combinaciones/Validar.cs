﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Nanpa_Kama.Core.Combinaciones
{
    public class Validar
    {
        private int maxCoincidencias = 5;
        private string etLista = string.Empty;
        private string etHistorico = string.Empty;
        private Helper.Utility util = new Helper.Utility();
        private Core.Combinaciones.Data data = Core.Combinaciones.Data.GetInstance();


        public string ElapsedTimeLista
        {
            get
            {
                return etLista;
            }
        }

        public string ElapsedTimeHistorico
        {
            get
            {
                return etHistorico;
            }
        }

        public Validar()
        {

        }

        public bool Historico(Model.Data.Combinacion comb)
        {
            bool result = false;

            result = ValidarHistoricoPrimitiva(comb);

            if (comb.IsValid)
            {
                comb.IsValid = result;
            }

            return result;
        }

        private bool ValidarHistoricoPrimitiva(Model.Data.Combinacion comb)
        {
            bool result = true;
            bool exit4 = false;
            int coincidencias = 0;
            List<Model.t_combinaciones_historico> sublst1 = data.lstCombinacionesHistorico.Where(w => w.d1 == comb.d1).ToList();
            Stopwatch stopWatch = new Stopwatch();

            stopWatch.Start();

            if (sublst1.Count >= 1)
            {
                foreach (Model.t_combinaciones_historico combHrco in sublst1)
                {
                    coincidencias = 0;
                    exit4 = false;
                    Model.Data.Combinacion combH = util.ToCombinacioBase(combHrco);

                    foreach (int item in combH.ArrayInt())
                    {
                        if (comb.ArrayInt().ToList().Contains(item))
                        {
                            coincidencias += 1;
                        }

                        if (coincidencias >= maxCoincidencias)
                        {
                            result = false;
                            exit4 = true;
                            break;
                        }
                    }

                    if (exit4)
                    {
                        break;
                    }
                }
            }

            if (comb.IsValid)
            {
                comb.IsValid = result;
            }

            stopWatch.Stop();
            TimeSpan time = stopWatch.Elapsed;
            etHistorico = string.Format("{0:00}:{1:00}:{2:00}.{3:00}", time.Hours, time.Minutes, time.Seconds, time.Milliseconds / 10);

            return result;
        }

        public bool Lista(Model.Data.Combinacion comb, List<Model.Data.Combinacion> lst)
        {
            bool result = true;

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            int count = (from Model.Data.Combinacion c in lst
                         where c.d1 == comb.d1
                         && c.d2 == comb.d2
                         && c.d3 == comb.d3
                         && c.d4 == comb.d4
                         && c.d5 == comb.d5
                         && c.d6 == comb.d6
                         select c).Count();

            if (count >= 1)
            {
                result = false;
            }

            if (comb.IsValid)
            {
                comb.IsValid = result;
            }

            stopWatch.Stop();
            TimeSpan time = stopWatch.Elapsed;
            etLista = string.Format("{0:00}:{1:00}:{2:00}.{3:00}", time.Hours, time.Minutes, time.Seconds, time.Milliseconds / 10);

            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nanpa_Kama.Core.Combinaciones
{
    public sealed class Data
    {
        private static Data instance = null;

        public List<Model.t_combinaciones_historico> lstCombinacionesHistorico = new List<Model.t_combinaciones_historico> { };
        public List<Model.t_combinaciones> lstCombinaciones = new List<Model.t_combinaciones> { };

        private Data()
        {
            GetData();
        }

        public static Data GetInstance()
        {
            if (instance == null)
            {
                instance = new Data();
            }

            return instance;
        }

        private void GetData()
        {
            using (Model.Nanpa_Kama_Model context = new Model.Nanpa_Kama_Model())
            {
                lstCombinacionesHistorico = context.t_combinaciones_historico.Select(s => s).ToList();
                lstCombinaciones = context.t_combinaciones.Select(s => s).ToList();
            }
        }
    }
}

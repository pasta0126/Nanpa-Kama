﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nanpa_Kama.Core.Combinaciones
{
    public class Filtros
    {
        Helper.Utility util = new Helper.Utility();

        public Filtros()
        {

        }

        public bool ParImpar(Model.Data.Combinacion comb, int maxCoincidencias, bool pares)
        {
            bool result = true;
            int coindencias = 0;

            for (int i = 1; i < 6; i++)
            {
                int c = 0;

                switch (i)
                {
                    case 1:
                        c = comb.d1;
                        break;

                    case 2:
                        c = comb.d2;
                        break;

                    case 3:
                        c = comb.d3;
                        break;

                    case 4:
                        c = comb.d4;
                        break;

                    case 5:
                        c = comb.d5;
                        break;

                    case 6:
                        c = comb.d6;
                        break;

                    default:
                        break;
                }

                if (pares)
                {
                    if (c % 2 == 0)
                    {
                        coindencias += 1;
                    }
                }
                else
                {
                    if (c % 2 != 0)
                    {
                        coindencias += 1;
                    }
                }
            }

            if (coindencias >= maxCoincidencias)
            {
                result = false;
            }

            return result;
        }

        public bool Seguidos(Model.Data.Combinacion comb, int maxCoincidencias, int distancia)
        {
            bool result = true;
            int coindencias = 0;

            for (int i = 1; i < 6; i++)
            {
                int c = 0;

                switch (i)
                {
                    case 1:
                        c = comb.d1;
                        break;

                    case 2:
                        c = comb.d2;
                        break;

                    case 3:
                        c = comb.d3;
                        break;

                    case 4:
                        c = comb.d4;
                        break;

                    case 5:
                        c = comb.d5;
                        break;

                    case 6:
                        c = comb.d6;
                        break;

                    default:
                        break;
                }

                for (int j = i; j < 6; j++)
                {
                    switch (j)
                    {
                        case 1:
                            if (c + distancia == comb.d2 ||
                                c + distancia == comb.d3 ||
                                c + distancia == comb.d4 ||
                                c + distancia == comb.d5 ||
                                c + distancia == comb.d6)
                            {
                                coindencias += 1;
                            }
                            break;

                        case 2:
                            if (c + distancia == comb.d3 ||
                                c + distancia == comb.d4 ||
                                c + distancia == comb.d5 ||
                                c + distancia == comb.d6)
                            {
                                coindencias += 1;
                            }
                            break;

                        case 3:
                            if (c + distancia == comb.d4 ||
                                c + distancia == comb.d5 ||
                                c + distancia == comb.d6)
                            {
                                coindencias += 1;
                            }
                            break;

                        case 4:
                            if (c + distancia == comb.d5 ||
                                c + distancia == comb.d6)
                            {
                                coindencias += 1;
                            }
                            break;

                        case 5:
                            if (c + distancia == comb.d6)
                            {
                                coindencias += 1;
                            }
                            break;

                        case 6:
                            break;

                        default:
                            break;
                    }

                }

            }

            if (coindencias >= maxCoincidencias)
            {
                result = false;
            }

            return result;
        }

        public bool Decenas3y3()
        {
            bool result = false;

            return result;
        }
    }
}

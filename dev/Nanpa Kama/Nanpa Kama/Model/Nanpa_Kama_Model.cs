namespace Nanpa_Kama.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Nanpa_Kama_Model : DbContext
    {
        public Nanpa_Kama_Model()
            : base("name=ConnStr")
        {
        }

        public virtual DbSet<t_combinaciones> t_combinaciones { get; set; }
        public virtual DbSet<t_combinaciones_historico> t_combinaciones_historico { get; set; }
        public virtual DbSet<t_jugadores_sorteos> t_jugadores_sorteos { get; set; }
        public virtual DbSet<t_master_jugadores> t_master_jugadores { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<t_combinaciones>()
                .Property(e => e.comentario)
                .IsUnicode(false);

            modelBuilder.Entity<t_jugadores_sorteos>()
                .Property(e => e.tipo_sorteo)
                .IsUnicode(false);

            modelBuilder.Entity<t_master_jugadores>()
                .Property(e => e.jugador)
                .IsUnicode(false);
        }
    }
}

namespace Nanpa_Kama.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("nanpa_kama.t_jugadores_sorteos")]
    public partial class t_jugadores_sorteos
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id_jugador { get; set; }

        [Key]
        [Column(Order = 1, TypeName = "date")]
        public DateTime fecha_sorteo { get; set; }

        [Required]
        [StringLength(20)]
        public string tipo_sorteo { get; set; }

        public int participacion_euros_sorteo { get; set; }

        public sbyte pagado { get; set; }
    }
}

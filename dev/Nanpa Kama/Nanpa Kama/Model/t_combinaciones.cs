namespace Nanpa_Kama.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("nanpa_kama.t_combinaciones")]
    public partial class t_combinaciones
    {
        public int id { get; set; }

        public int d1 { get; set; }

        public int d2 { get; set; }

        public int d3 { get; set; }

        public int d4 { get; set; }

        public int d5 { get; set; }

        public int d6 { get; set; }

        public int? complementario { get; set; }

        public float? joker { get; set; }

        public DateTime? fecha_generado { get; set; }

        [StringLength(255)]
        public string comentario { get; set; }

        public int premiado { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fecha_sorteo_1 { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fecha_sorteo_2 { get; set; }
    }
}

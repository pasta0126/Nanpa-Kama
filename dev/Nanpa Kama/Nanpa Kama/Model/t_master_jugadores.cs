namespace Nanpa_Kama.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("nanpa_kama.t_master_jugadores")]
    public partial class t_master_jugadores
    {
        public int id { get; set; }

        [Required]
        [StringLength(255)]
        public string jugador { get; set; }

        public int participacion_euros_sorteo { get; set; }

        [Column(TypeName = "date")]
        public DateTime fecha_alta { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fecha_baja { get; set; }
    }
}

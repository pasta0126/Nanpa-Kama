﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nanpa_Kama.Model.Data
{
    public class JugadorSorteo
    {
        public int id_jugador { get; set; }
        public string jugador { get; set; }
        public DateTime fecha_sorteo { get; set; }
        public int participacion_euros_sorteo { get; set; }
        public sbyte pagado { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nanpa_Kama.Model.Data
{
    public class Combinacion
    {
        public bool IsValid { get; set; }
        public int d1 { get; set; }
        public int d2 { get; set; }
        public int d3 { get; set; }
        public int d4 { get; set; }
        public int d5 { get; set; }
        public int d6 { get; set; }
        public int? complementario { get; set; }
        public float? joker { get; set; }

        public Combinacion()
        {
            IsValid = true;
        }

        public string Cadena()
        {
            return string.Format("{0},{1},{2},{3},{4},{5}", d1, d2, d3, d4, d5, d6);
        }

        public string[] ArrayStr()
        {
            return Cadena().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        }

        public int[] ArrayInt()
        {
            string[] arr = ArrayStr();
            int[] result = new int[arr.Length - 1];

            for (int i = 0; i < arr.Length - 1; i++)
            {
                result[i] = Convert.ToInt32(arr[i]);
            }

            return result;
        }

        public bool Contains(int num)
        {
            if (d1 == num)
            {
                return true;
            }

            if (d2 == num)
            {
                return true;
            }

            if (d3 == num)
            {
                return true;
            }

            if (d4 == num)
            {
                return true;
            }

            if (d5 == num)
            {
                return true;
            }

            if (d6 == num)
            {
                return true;
            }

            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nanpa_Kama.Helper
{
    public class Log
    {
        private Helper.Utility util = new Utility();
        private TextBox txt;
        private string filePath = string.Empty;

        public Log()
        {
            Init();
        }

        public Log(TextBox t)
        {
            Init();
            txt = t;
        }

        public void Add(string message)
        {
            string msg = string.Empty;

            msg += DateTime.Now.ToString("yyyyMMdd-hhmmss");
            msg += "|";
            msg += message;
            msg += "\r\n";

            LogAddTxt(msg);
            Console.WriteLine(msg);

            txt.Refresh();
        }

        private void Init()
        {
            filePath = GenerateFilePath();
        }

        private string GenerateFilePath()
        {
            string folderPath = Path.GetDirectoryName(util.AppPath()) + "\\" + "log";

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            string completeFilepath = folderPath + "\\" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + "Nanpa-Kama_Log.txt";

            using (TextWriter tw = new StreamWriter(completeFilepath, true))
            {
                tw.WriteLine(string.Empty);
            }

            return completeFilepath;
        }

        private void LogAddTxt(string message)
        {
            txt.AppendText(message);
            LogAddFile(message);
        }

        private void LogAddFile(string message)
        {
            using (TextWriter tw = new StreamWriter(filePath, true))
            {
                tw.WriteLine(message);
            }
        }
    }
}

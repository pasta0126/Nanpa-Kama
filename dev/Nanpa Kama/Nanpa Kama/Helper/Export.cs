﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data.OleDb;
namespace Nanpa_Kama.Helper
{
    public class Export
    {
        public Export()
        {

        }

        public bool ToExcel(DataTable dt, string filePath)
        {
            bool result = false;

            if (dt != null)
            {
                var excelApp = new Excel.Application();

                excelApp.Workbooks.Add();// load excel, and create a new workbook
                Excel._Worksheet workSheet = excelApp.ActiveSheet;// single worksheet

                // headers
                for (var i = 0; i < dt.Columns.Count; i++)
                {
                    workSheet.Cells[1, i + 1] = dt.Columns[i].ColumnName;
                }

                // rows
                for (var i = 0; i < dt.Rows.Count; i++)
                {
                    // to do: format datetime values before printing
                    for (var j = 0; j < dt.Columns.Count; j++)
                    {
                        workSheet.Cells[i + 2, j + 1] = dt.Rows[i][j];
                    }
                }

                // save or visible
                if (!string.IsNullOrEmpty(filePath))
                {
                    try
                    {
                        workSheet.SaveAs(filePath);
                        excelApp.Quit();
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("ExportToExcel: Excel file could not be saved! Check filepath.\n"
                                            + ex.Message);
                    }
                }
                else
                {
                    excelApp.Visible = true;
                }
            }

            return result;
        }

        //public void ToCSV(DataTable dt, string filePath)
        //{
        //    var lines = new List<string>();
            
        //    string[] columnNames = dt.Columns.Cast<DataColumn>().Select(column => column.ColumnName).ToArray();

        //    var header = string.Join(",", columnNames);
        //    lines.Add(header);

        //    var valueLines = dt.AsEnumerable()
        //                       .Select(row => string.Join(",", row.ItemArray));
        //    lines.AddRange(valueLines);

        //    File.WriteAllLines("excel.csv", lines);
        //}

    }
}

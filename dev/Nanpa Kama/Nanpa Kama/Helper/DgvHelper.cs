﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nanpa_Kama.Helper
{
    public class DgvHelper
    {
        public string ColumName;
        public string Caption;
        public int MinWidth;
        public DgvColumnType.ColumnType DgvColumn;
        public List<string> LstComboValues;
        public Dictionary<int, string> DictComboValues;
        public bool Editable;
        public bool Visible;

        public DgvHelper(string ColumName, string Caption, int MinWidth, DgvColumnType.ColumnType DgvColumn, List<string> LstComboValues, bool Editable, bool Visible)
        {
            this.ColumName = ColumName;
            this.Caption = Caption;
            this.MinWidth = MinWidth;
            this.DgvColumn = DgvColumn;
            this.LstComboValues = LstComboValues;
            this.Editable = Editable;
            this.Visible = Visible;
        }

        public DgvHelper(string ColumName, string Caption, int MinWidth, DgvColumnType.ColumnType DgvColumn, Dictionary<int, string> DictComboValues, bool Editable, bool Visible)
        {
            this.ColumName = ColumName;
            this.Caption = Caption;
            this.MinWidth = MinWidth;
            this.DgvColumn = DgvColumn;
            this.DictComboValues = DictComboValues;
            this.Editable = Editable;
            this.Visible = Visible;
        }

        public DgvHelper(string ColumName, string Caption, int MinWidth, bool Editable, bool Visible)
        {
            this.ColumName = ColumName;
            this.Caption = Caption;
            this.MinWidth = MinWidth;
            this.Editable = Editable;
            this.Visible = Visible;
        }
    }
}

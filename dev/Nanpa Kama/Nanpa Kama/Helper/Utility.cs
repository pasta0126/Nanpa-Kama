﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Deployment.Application;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using Nanpa_Kama.Model;
using Nanpa_Kama.Model.Data;

namespace Nanpa_Kama.Helper
{
    public class Utility
    {
        public readonly string Alpha = "α";
        public readonly string Beta = "β";

        public DataTable addDtColumns(string[] nombres)
        {
            DataTable dt = new DataTable();
            foreach (string nombre in nombres)
            {
                dt.Columns.Add(nombre);
            }
            return dt;
        }

        public bool CrearDirectorio(string directorio)
        {
            try
            {
                Directory.CreateDirectory(directorio);
                return true;
            }
            catch (Exception)
            {
                //GBL.log.addLog(ex.Message + " | " + ex.TargetSite.Name, Log.tipoLog.Crash);
                return false;
            }
        }

        public bool VaciarDirectorio(string directorio)
        {
            try
            {
                Directory.Delete(directorio, true);
                return true;
            }
            catch (Exception)
            {
                //GBL.log.addLog(ex.Message + " | " + ex.TargetSite.Name, Log.tipoLog.Crash);
                return false;
            }
        }

        public static class FirstDayOfWeekUtility
        {
            /// <summary>
            /// Returns the first day of the week that the specified
            /// date is in using the current culture. 
            /// </summary>
            public static DateTime GetFirstDayOfWeek(DateTime dayInWeek)
            {
                CultureInfo defaultCultureInfo = CultureInfo.CurrentCulture;
                return GetFirstDayOfWeek(dayInWeek, defaultCultureInfo);
            }

            /// <summary>
            /// Returns the first day of the week that the specified date 
            /// is in. 
            /// </summary>
            public static DateTime GetFirstDayOfWeek(DateTime dayInWeek, CultureInfo cultureInfo)
            {
                DayOfWeek firstDay = cultureInfo.DateTimeFormat.FirstDayOfWeek;
                DateTime firstDayInWeek = dayInWeek.Date;
                while (firstDayInWeek.DayOfWeek != firstDay)
                    firstDayInWeek = firstDayInWeek.AddDays(-1);

                return firstDayInWeek;
            }
        }

        public Combinacion ToCombinacioBase(Model.Data.Combinacion combinacion)
        {
            Combinacion result = new Combinacion();

            result.d1 = combinacion.d1;
            result.d2 = combinacion.d2;
            result.d3 = combinacion.d3;
            result.d4 = combinacion.d4;
            result.d5 = combinacion.d5;
            result.d6 = combinacion.d6;

            return result;
        }

        public Combinacion ToCombinacioBase(t_combinaciones_historico combHrco)
        {
            Combinacion result = new Combinacion();

            result.d1 = combHrco.d1;
            result.d2 = combHrco.d2;
            result.d3 = combHrco.d3;
            result.d4 = combHrco.d4;
            result.d5 = combHrco.d5;
            result.d6 = combHrco.d6;

            return result;
        }

        //////////////////////
        // http://www.chinhdo.com/20090402/convert-list-to-datatable/
        //////////////////////
        /// <summary>
        /// Convert a List{T} to a DataTable.
        /// </summary>
        public DataTable ToDataTable<T>(List<T> items)
        {
            var tb = new DataTable(typeof(T).Name);

            PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo prop in props)
            {
                Type t = GetCoreType(prop.PropertyType);
                tb.Columns.Add(prop.Name, t);
            }


            foreach (T item in items)
            {
                var values = new object[props.Length];

                for (int i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(item, null);
                }

                tb.Rows.Add(values);
            }

            return tb;
        }

        public DataTable ToDataTable(List<string> lst, string colName, bool readOnly)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add(colName);
            dt.Columns[colName].ReadOnly = readOnly;

            foreach (string s in lst)
            {
                dt.Rows.Add(s);
            }

            return dt;
        }

        /// <summary>
        /// Determine of specified type is nullable
        /// </summary>
        public static bool IsNullable(Type t)
        {
            return !t.IsValueType || (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>));
        }

        /// <summary>
        /// Return underlying type if type is Nullable otherwise return the type
        /// </summary>
        public static Type GetCoreType(Type t)
        {
            if (t != null && IsNullable(t))
            {
                if (!t.IsValueType)
                {
                    return t;
                }
                else
                {
                    return Nullable.GetUnderlyingType(t);
                }
            }
            else
            {
                return t;
            }
        }
        //////////////////////

        //public void SetDS(List<BindingSource> lst)
        //{
        //    foreach (BindingSource bs in lst)
        //    {
        //        bs.AllowNew = true;
        //    }
        //}

        public string AppPath()
        {
            return Application.ExecutablePath;
        }

        public bool EsEntero(string valor)
        {
            if (!string.IsNullOrEmpty(valor))
            {
                int val;
                return int.TryParse(valor, out val);
            }
            else return false;
        }

        public string AppVersion()
        {
            Version ver = Assembly.GetExecutingAssembly().GetName().Version;
            string result = string.Empty;

            if (ApplicationDeployment.IsNetworkDeployed)
            {
                ver = ApplicationDeployment.CurrentDeployment.CurrentVersion;
            }

            //#if !DEBUG
            //            try
            //            {
            //                result = ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
            //            }
            //            catch (Exception e)
            //            {

            //            }
            //#endif

            result = string.Format("{0}.{1}.{2}.{3}", ver.Major, ver.Minor, ver.Build, ver.Revision);

            return result;
        }

        public DataTable ListToTable()
        {
            DataTable res = new DataTable();



            return res;
        }

        public int Porcentage(int valor, int maxValor)
        {
            return Convert.ToInt32((valor * 100) / maxValor);
        }

        public bool IsFormOpen(string formName, bool toFront)
        {
            bool result = false;

            FormCollection fc = Application.OpenForms;

            foreach (Form frm in fc)
            {
                if (frm.Name == formName)
                {
                    if (toFront)
                    {
                        frm.BringToFront();
                    }

                    result = true;
                    break;
                }
            }

            return result;
        }

        public bool IsOdd(int value)
        {
            return value % 2 != 0;
        }
    }
}

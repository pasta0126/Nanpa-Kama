﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nanpa_Kama
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            View.frm_Splash frmSplash = new View.frm_Splash();
            frmSplash.Show();
            Application.Run();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.Message);
            CreateCrashLog(e.Exception.Message);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show((e.ExceptionObject as Exception).Message);
            CreateCrashLog((e.ExceptionObject as Exception).Message);
        }

        /// <summary>
        /// Creación de crash errors a nivel de aplicación
        /// </summary>
        /// <param name="message">Se guarda el mensaje de error de la excepción generada</param>
        private static void CreateCrashLog(string message)
        {
            //GBL.log.addLog(message, Log.tipoLog.Crash);
            //ShutDownApp();
        }

        /// <summary>
        /// 
        /// </summary>
        private static void ShutDownApp()
        {
            Application.Exit();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nanpa_Kama.Manager
{
    public class Datagridview
    {
        private string prefixColumCombobox = "cmb_";

        public Datagridview()
        {

        }

        public void AddCustomColumns(DataGridView dgv, List<Helper.DgvHelper> lstCols)
        {
            foreach (Helper.DgvHelper dgvH in lstCols)
            {
                switch (dgvH.DgvColumn)
                {
                    case Helper.DgvColumnType.ColumnType.Combobox:
                        CreateComboboxColumn(dgv, dgvH);
                        break;

                    default:
                        break;
                }
            }
        }

        private void CreateComboboxColumn(DataGridView dgv, Helper.DgvHelper dgvH)
        {
            DataGridViewComboBoxColumn col = new DataGridViewComboBoxColumn();
            DataGridViewComboBoxCell cell = new DataGridViewComboBoxCell();

            col.Name = prefixColumCombobox + dgvH.ColumName;
            col.HeaderText = dgvH.Caption;
            col.MinimumWidth = dgvH.MinWidth;
            col.CellTemplate = cell;

            // alimentado por lista
            if (dgvH.LstComboValues != null)
            {
                col.DataSource = dgvH.LstComboValues;
            }

            // alimentado por diccionario
            if (dgvH.DictComboValues != null)
            {
                col.DisplayMember = "value";
                col.ValueMember = "id";

                DataTable dt = new DataTable();

                dt.Columns.Add("id");
                dt.Columns.Add("value");

                foreach (KeyValuePair<int, string> kv in dgvH.DictComboValues)
                {
                    dt.Rows.Add(kv.Key, kv.Value);
                }

                col.DataSource = dt;
            }

            // agregamos columna
            dgv.Columns.Add(col);
        }

        public void OrderAndVisibleColumns(DataGridView dgv, List<Helper.DgvHelper> lstCols)
        {
            int index = 0;

            foreach (DataGridViewColumn col in dgv.Columns)
            {
                col.Visible = false;
            }


            foreach (Helper.DgvHelper dgvH in lstCols)
            {
                DataGridViewColumn col = dgv.Columns[dgvH.ColumName];

                if (dgvH.DgvColumn == Helper.DgvColumnType.ColumnType.Combobox)
                {
                    col = dgv.Columns[prefixColumCombobox + dgvH.ColumName];
                }

                col.Visible = dgvH.Visible;
                col.HeaderText = dgvH.Caption;
                col.Width = dgvH.MinWidth;
                col.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                col.ReadOnly = !dgvH.Editable;
                col.DisplayIndex = index;

                //if (!dgvH.Editable)
                //{
                //    col.DisplayIndex = index + 1;
                //    col.DisplayIndex = index;
                //}
                //else
                //{
                //    col.DisplayIndex = index;
                //}

                index += 1;
            }
        }

        public void SetValueCustomColumns(DataGridView dgv, List<Helper.DgvHelper> lstCols)
        {
            foreach (Helper.DgvHelper dgvH in lstCols)
            {
                if (dgvH.DgvColumn == Helper.DgvColumnType.ColumnType.Combobox)
                {
                    foreach (DataGridViewRow row in dgv.Rows)
                    {
                        if (row.Cells[dgvH.ColumName].Value != null)
                        {
                            //DataGridViewComboBoxCell cmbCell = (DataGridViewComboBoxCell)row.Cells[prefixColumCombobox + dgvH.ColumName];

                            //cmbCell.Value = row.Cells[dgvH.ColumName].Value.ToString();

                            row.Cells[prefixColumCombobox + dgvH.ColumName].Value = row.Cells[dgvH.ColumName].Value.ToString();
                        }
                    }
                }
            }
        }
    }
}

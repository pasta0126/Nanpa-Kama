﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Nanpa_Kama.Manager
{
    public class UI
    {
        private Color txt_Backcground_OK = SystemColors.Window;
        private Color txt_Backcground_Empty = System.Drawing.Color.BlanchedAlmond;

        public UI()
        {

        }

        public void FormStyler(Form form, string title)
        {
            form.Text = title;
            form.Icon = Properties.Resources.ico_icon;
            //form.StartPosition = FormStartPosition.CenterParent;
            //form.WindowState = FormWindowState.Maximized;
            form.ShowInTaskbar = true;
            form.FormBorderStyle = FormBorderStyle.Sizable;
            form.KeyPreview = true;
        }

        public void FormStyler(Form form, View.frm_Main mdi, string title)
        {
            form.Name = title; // todo: Revisar que hacer con el título
            form.ShowInTaskbar = true;
            form.FormBorderStyle = FormBorderStyle.Sizable;
            form.KeyPreview = true;
            form.MdiParent = mdi;
        }

        public void TXTOK(TextBox txt)
        {
            txt.BackColor = txt_Backcground_OK;
        }

        public void TXTEmpty(TextBox txt)
        {
            txt.BackColor = txt_Backcground_Empty;
        }

        public void StyleDG(List<DataGridView> lstDgs)
        {
            foreach (DataGridView dg in lstDgs)
            {
                dg.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
                //dg.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                dg.BorderStyle = BorderStyle.Fixed3D;
                dg.EditMode = DataGridViewEditMode.EditOnKeystroke;
                dg.AllowUserToAddRows = false;
                dg.AllowUserToDeleteRows = false;
                //dg.AllowUserToResizeColumns = true;
                dg.AutoGenerateColumns = true;
                dg.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dg.RowsDefaultCellStyle.BackColor = Color.WhiteSmoke;
                dg.AlternatingRowsDefaultCellStyle.BackColor = Color.PowderBlue;

                //dg.Refresh();
            }
        }

        internal void CMBOK(ComboBox cmb)
        {
            cmb.BackColor = txt_Backcground_OK;
        }

        internal void CMBEmpty(ComboBox cmb)
        {
            cmb.BackColor = txt_Backcground_Empty;
        }

        public void StyleCmb(List<ComboBox> lstCmbs)
        {
            foreach (ComboBox cmb in lstCmbs)
            {
                cmb.AutoCompleteMode = AutoCompleteMode.Append;
                cmb.AutoCompleteSource = AutoCompleteSource.ListItems;
                // cmb.DropDownStyle = ComboBoxStyle.DropDownList;
            }
        }

        internal void DTPOK(DateTimePicker dtp)
        {

        }

        internal void DTPKO(DateTimePicker dtp)
        {

        }

        public void StyleForm(Form f, Icon icon, string title)
        {
            f.Text = title;

            // Default icon is empty icon
            f.Icon = Properties.Resources.ico_icon;

            if (icon != null)
            {
                f.Icon = icon;
            }

            f.ShowInTaskbar = true;
            f.StartPosition = FormStartPosition.CenterParent;
            f.WindowState = FormWindowState.Maximized;
            f.KeyPreview = true;
        }

        public void StyleSplash(Form f)
        {
            f.Icon = Nanpa_Kama.Properties.Resources.ico_icon;
            f.ShowInTaskbar = true;
            f.StartPosition = FormStartPosition.CenterScreen;
            f.KeyPreview = true;
            f.MinimizeBox = false;
            f.MaximizeBox = false;
        }

        public void CenterOnScreen(Form f)
        {
            f.Location = new Point((Screen.PrimaryScreen.Bounds.Size.Width / 2) - (f.Size.Width / 2), (Screen.PrimaryScreen.Bounds.Size.Height / 2) - (f.Size.Height / 2));
        }

        public void LimpiarControles(List<Control> controles)
        {
            foreach (Control ctl in controles)
            {
                LimpiarControl(ctl);
            }
        }

        public void LimpiarControl(Control ctl)
        {
            switch (ctl.GetType().ToString())
            {
                case "System.Windows.Forms.TextBox":
                    TextBox txt = (TextBox)ctl;
                    txt.Text = string.Empty;
                    break;

                case "System.Windows.Forms.MaskedTextBox":
                    MaskedTextBox mtxt = (MaskedTextBox)ctl;
                    Console.WriteLine(mtxt.Name + " --> " + mtxt.Text);
                    mtxt.Text = null;
                    break;

                case "System.Windows.Forms.ComboBox":
                    ComboBox cmb = (ComboBox)ctl;
                    cmb.SelectedIndex = -1;
                    break;

                case "System.Windows.Forms.DateTimePicker":
                    DateTimePicker dtp = (DateTimePicker)ctl;
                    dtp.Value = DateTime.Today;
                    break;

                case "System.Windows.Forms.DataGridView":
                    DataGridView dgv = (DataGridView)ctl;
                    dgv.Rows.Clear();
                    break;

                default:
                    break;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nanpa_Kama.Manager
{
    public class Progress
    {
        ProgressBar prgBar;
        Label lblInfo = null;
        TextBox txtInfo;
        ToolStripStatusLabel lblStatus = null;
        int maxElementos = 0;
        int elementos = 0;

        public Progress()
        {
            initProgress();
        }

        public Progress(ProgressBar pBar, TextBox tInfo)
        {
            prgBar = pBar;
            txtInfo = tInfo;

            initProgress();
        }

        private void initProgress()
        {
            UpdateProgress();
        }

        public void UpdateProgress()
        {
            UpdateProgress(string.Empty, 0);
        }

        public void UpdateProgress(int valor)
        {
            UpdateProgress(string.Empty, valor);
        }

        public void UpdateProgress(string message, int valor)
        {
            UpdatePBar(valor);
            UpdateInfo(message);
            UpdateStatus();
        }

        private void UpdateStatus()
        {
            if (lblStatus != null)
            {
                lblStatus.Text = string.Format("{0} de {1}", elementos, maxElementos);
            }
        }

        private void UpdateInfo(string message)
        {
            if (lblInfo != null)
            {
                lblInfo.Text = message;
            }

            if (txtInfo != null)
            {
                txtInfo.Text = message;
                txtInfo.Refresh();
            }
        }

        private void UpdatePBar(int valor)
        {
            if (prgBar != null)
            {
                prgBar.Value = valor;
            }
        }
    }
}

﻿using System;
using System.Windows.Forms;

namespace Nanpa_Kama.View
{
    public partial class frm_Login : Form
    {
        //private bool continuar = false;

        public frm_Login()
        {
            InitializeComponent();
        }

        private void frm_Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (!continuar)
            //{
            //    Application.Exit();
            //}
        }

        private void frm_Login_Load(object sender, EventArgs e)
        {
            this.Close();

            View.frm_Main frmMain = new frm_Main();
            frmMain.Show();
        }
    }
}

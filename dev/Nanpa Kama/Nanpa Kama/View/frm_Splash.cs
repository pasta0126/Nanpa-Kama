﻿using System;
using System.Windows.Forms;

namespace Nanpa_Kama.View
{
    public partial class frm_Splash : Form
    {
        private System.Windows.Forms.Timer timer;
        private int interval = 1000;
        private int splashTime = 0;
        private int splashMinTime = 2;
        private Manager.UI ui = new Manager.UI();
        private bool isFinished = false;

        public frm_Splash()
        {
            InitializeComponent();
        }

        private void frm_Splash_Load(object sender, EventArgs e)
        {
            timer = new System.Windows.Forms.Timer();
            timer.Tick += new EventHandler(timer_tick);
            timer.Interval = interval;
            timer.Enabled = true;
            timer.Start();

            ui.StyleSplash(this);
            ui.CenterOnScreen(this);

            LoadData();

            isFinished = true;
        }

        private void LoadData()
        {
            Core.Combinaciones.Data data = Core.Combinaciones.Data.GetInstance();
        }

        private void timer_tick(object sender, System.EventArgs e)
        {
            if (splashTime < splashMinTime)
            {
                splashTime += 1;

                if (!isFinished)
                {
                    splashTime -= 1;
                }

                prgLoad.Value = (splashTime / splashMinTime) * 100;
            }
            else
            {
                timer.Stop();
                this.Close();

                View.frm_Login frmLogin = new frm_Login();
                frmLogin.Show();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Nanpa_Kama.View
{
    public partial class frm_Main : Form
    {
        private Manager.UI ui = new Manager.UI();
        private Helper.Utility util = new Helper.Utility();

        public frm_Main()
        {
            InitializeComponent();
        }

        private void frm_Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void frm_Main_Load(object sender, EventArgs e)
        {
            InitForm();
        }

        private void InitForm()
        {
            ui.StyleForm(this, Properties.Resources.ico_icon, Application.ProductName + " " + util.AppVersion());

            notifIcon.Text = Application.ProductName;
            notifIcon.BalloonTipTitle = Application.ProductName;
            notifIcon.BalloonTipText = string.Format("Hola {0}", Environment.UserName);
            notifIcon.ShowBalloonTip(1);
        }

        #region menu

        private void combinacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_Combinaciones frm = new frm_Combinaciones();

            if (!util.IsFormOpen(frm.Name, true))
            {
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void históricoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_Historico frm = new frm_Historico();

            if (!util.IsFormOpen(frm.Name, true))
            {
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void jugadoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_Jugadores frm = new frm_Jugadores();

            if (!util.IsFormOpen(frm.Name, true))
            {
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void sorteosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        #endregion

        private void apuestasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_Apuestas frm = new frm_Apuestas();

            if (!util.IsFormOpen(frm.Name, true))
            {
                frm.MdiParent = this;
                frm.Show();
            }
        }
    }
}

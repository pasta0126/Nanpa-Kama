﻿namespace Nanpa_Kama.View
{
    partial class frm_Historico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grbDatos = new System.Windows.Forms.GroupBox();
            this.txtBuscarD6 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBuscarD5 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBuscarD4 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBuscarD3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBuscarD2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBuscarD1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnCargarHistorico = new System.Windows.Forms.Button();
            this.dgvDatos = new System.Windows.Forms.DataGridView();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.stslblInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.grbCombinacion = new System.Windows.Forms.GroupBox();
            this.lblSorteo = new System.Windows.Forms.Label();
            this.cmbSorteo = new System.Windows.Forms.ComboBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.lblFechaSorteo = new System.Windows.Forms.Label();
            this.dtpFechaSorteo = new System.Windows.Forms.DateTimePicker();
            this.txtJoker = new System.Windows.Forms.TextBox();
            this.lblJoker = new System.Windows.Forms.Label();
            this.txtComplementario = new System.Windows.Forms.TextBox();
            this.lblComp = new System.Windows.Forms.Label();
            this.txtD6 = new System.Windows.Forms.TextBox();
            this.lblD6 = new System.Windows.Forms.Label();
            this.txtD5 = new System.Windows.Forms.TextBox();
            this.lblD5 = new System.Windows.Forms.Label();
            this.txtD4 = new System.Windows.Forms.TextBox();
            this.lblD4 = new System.Windows.Forms.Label();
            this.txtD3 = new System.Windows.Forms.TextBox();
            this.lblD3 = new System.Windows.Forms.Label();
            this.txtD2 = new System.Windows.Forms.TextBox();
            this.lblD2 = new System.Windows.Forms.Label();
            this.txtD1 = new System.Windows.Forms.TextBox();
            this.lblD1 = new System.Windows.Forms.Label();
            this.grbDatos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatos)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.grbCombinacion.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbDatos
            // 
            this.grbDatos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbDatos.Controls.Add(this.txtBuscarD6);
            this.grbDatos.Controls.Add(this.label1);
            this.grbDatos.Controls.Add(this.txtBuscarD5);
            this.grbDatos.Controls.Add(this.label2);
            this.grbDatos.Controls.Add(this.txtBuscarD4);
            this.grbDatos.Controls.Add(this.label3);
            this.grbDatos.Controls.Add(this.txtBuscarD3);
            this.grbDatos.Controls.Add(this.label4);
            this.grbDatos.Controls.Add(this.txtBuscarD2);
            this.grbDatos.Controls.Add(this.label5);
            this.grbDatos.Controls.Add(this.txtBuscarD1);
            this.grbDatos.Controls.Add(this.label6);
            this.grbDatos.Controls.Add(this.btnCargarHistorico);
            this.grbDatos.Controls.Add(this.dgvDatos);
            this.grbDatos.Location = new System.Drawing.Point(12, 89);
            this.grbDatos.Name = "grbDatos";
            this.grbDatos.Size = new System.Drawing.Size(703, 294);
            this.grbDatos.TabIndex = 1;
            this.grbDatos.TabStop = false;
            // 
            // txtBuscarD6
            // 
            this.txtBuscarD6.Location = new System.Drawing.Point(310, 30);
            this.txtBuscarD6.Name = "txtBuscarD6";
            this.txtBuscarD6.Size = new System.Drawing.Size(32, 20);
            this.txtBuscarD6.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(307, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "#6";
            // 
            // txtBuscarD5
            // 
            this.txtBuscarD5.Location = new System.Drawing.Point(272, 30);
            this.txtBuscarD5.Name = "txtBuscarD5";
            this.txtBuscarD5.Size = new System.Drawing.Size(32, 20);
            this.txtBuscarD5.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(269, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "#5";
            // 
            // txtBuscarD4
            // 
            this.txtBuscarD4.Location = new System.Drawing.Point(234, 30);
            this.txtBuscarD4.Name = "txtBuscarD4";
            this.txtBuscarD4.Size = new System.Drawing.Size(32, 20);
            this.txtBuscarD4.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(231, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "#4";
            // 
            // txtBuscarD3
            // 
            this.txtBuscarD3.Location = new System.Drawing.Point(196, 30);
            this.txtBuscarD3.Name = "txtBuscarD3";
            this.txtBuscarD3.Size = new System.Drawing.Size(32, 20);
            this.txtBuscarD3.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(193, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "#3";
            // 
            // txtBuscarD2
            // 
            this.txtBuscarD2.Location = new System.Drawing.Point(158, 30);
            this.txtBuscarD2.Name = "txtBuscarD2";
            this.txtBuscarD2.Size = new System.Drawing.Size(32, 20);
            this.txtBuscarD2.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(155, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "#2";
            // 
            // txtBuscarD1
            // 
            this.txtBuscarD1.Location = new System.Drawing.Point(120, 30);
            this.txtBuscarD1.Name = "txtBuscarD1";
            this.txtBuscarD1.Size = new System.Drawing.Size(32, 20);
            this.txtBuscarD1.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(117, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "#1";
            // 
            // btnCargarHistorico
            // 
            this.btnCargarHistorico.Location = new System.Drawing.Point(6, 28);
            this.btnCargarHistorico.Name = "btnCargarHistorico";
            this.btnCargarHistorico.Size = new System.Drawing.Size(108, 23);
            this.btnCargarHistorico.TabIndex = 11;
            this.btnCargarHistorico.Text = "Cargar histórico";
            this.btnCargarHistorico.UseVisualStyleBackColor = true;
            this.btnCargarHistorico.Click += new System.EventHandler(this.btnCargarHistorico_Click);
            // 
            // dgvDatos
            // 
            this.dgvDatos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatos.Location = new System.Drawing.Point(6, 57);
            this.dgvDatos.Name = "dgvDatos";
            this.dgvDatos.Size = new System.Drawing.Size(691, 231);
            this.dgvDatos.TabIndex = 1;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stslblInfo});
            this.statusStrip1.Location = new System.Drawing.Point(0, 386);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(727, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // stslblInfo
            // 
            this.stslblInfo.Name = "stslblInfo";
            this.stslblInfo.Size = new System.Drawing.Size(10, 17);
            this.stslblInfo.Text = " ";
            // 
            // grbCombinacion
            // 
            this.grbCombinacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbCombinacion.Controls.Add(this.lblSorteo);
            this.grbCombinacion.Controls.Add(this.cmbSorteo);
            this.grbCombinacion.Controls.Add(this.btnGuardar);
            this.grbCombinacion.Controls.Add(this.lblFechaSorteo);
            this.grbCombinacion.Controls.Add(this.dtpFechaSorteo);
            this.grbCombinacion.Controls.Add(this.txtJoker);
            this.grbCombinacion.Controls.Add(this.lblJoker);
            this.grbCombinacion.Controls.Add(this.txtComplementario);
            this.grbCombinacion.Controls.Add(this.lblComp);
            this.grbCombinacion.Controls.Add(this.txtD6);
            this.grbCombinacion.Controls.Add(this.lblD6);
            this.grbCombinacion.Controls.Add(this.txtD5);
            this.grbCombinacion.Controls.Add(this.lblD5);
            this.grbCombinacion.Controls.Add(this.txtD4);
            this.grbCombinacion.Controls.Add(this.lblD4);
            this.grbCombinacion.Controls.Add(this.txtD3);
            this.grbCombinacion.Controls.Add(this.lblD3);
            this.grbCombinacion.Controls.Add(this.txtD2);
            this.grbCombinacion.Controls.Add(this.lblD2);
            this.grbCombinacion.Controls.Add(this.txtD1);
            this.grbCombinacion.Controls.Add(this.lblD1);
            this.grbCombinacion.Location = new System.Drawing.Point(12, 12);
            this.grbCombinacion.Name = "grbCombinacion";
            this.grbCombinacion.Size = new System.Drawing.Size(703, 71);
            this.grbCombinacion.TabIndex = 0;
            this.grbCombinacion.TabStop = false;
            this.grbCombinacion.Text = "Nuevo registro";
            // 
            // lblSorteo
            // 
            this.lblSorteo.AutoSize = true;
            this.lblSorteo.Location = new System.Drawing.Point(479, 20);
            this.lblSorteo.Name = "lblSorteo";
            this.lblSorteo.Size = new System.Drawing.Size(43, 13);
            this.lblSorteo.TabIndex = 20;
            this.lblSorteo.Text = "Sorteos";
            // 
            // cmbSorteo
            // 
            this.cmbSorteo.FormattingEnabled = true;
            this.cmbSorteo.Location = new System.Drawing.Point(482, 37);
            this.cmbSorteo.Name = "cmbSorteo";
            this.cmbSorteo.Size = new System.Drawing.Size(121, 21);
            this.cmbSorteo.TabIndex = 9;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(609, 37);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 10;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // lblFechaSorteo
            // 
            this.lblFechaSorteo.AutoSize = true;
            this.lblFechaSorteo.Location = new System.Drawing.Point(373, 20);
            this.lblFechaSorteo.Name = "lblFechaSorteo";
            this.lblFechaSorteo.Size = new System.Drawing.Size(69, 13);
            this.lblFechaSorteo.TabIndex = 17;
            this.lblFechaSorteo.Text = "Fecha sorteo";
            // 
            // dtpFechaSorteo
            // 
            this.dtpFechaSorteo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaSorteo.Location = new System.Drawing.Point(376, 37);
            this.dtpFechaSorteo.Name = "dtpFechaSorteo";
            this.dtpFechaSorteo.Size = new System.Drawing.Size(100, 20);
            this.dtpFechaSorteo.TabIndex = 8;
            // 
            // txtJoker
            // 
            this.txtJoker.Location = new System.Drawing.Point(272, 37);
            this.txtJoker.Name = "txtJoker";
            this.txtJoker.Size = new System.Drawing.Size(98, 20);
            this.txtJoker.TabIndex = 7;
            // 
            // lblJoker
            // 
            this.lblJoker.AutoSize = true;
            this.lblJoker.Location = new System.Drawing.Point(269, 20);
            this.lblJoker.Name = "lblJoker";
            this.lblJoker.Size = new System.Drawing.Size(33, 13);
            this.lblJoker.TabIndex = 14;
            this.lblJoker.Text = "Joker";
            // 
            // txtComplementario
            // 
            this.txtComplementario.Location = new System.Drawing.Point(234, 37);
            this.txtComplementario.Name = "txtComplementario";
            this.txtComplementario.Size = new System.Drawing.Size(32, 20);
            this.txtComplementario.TabIndex = 6;
            // 
            // lblComp
            // 
            this.lblComp.AutoSize = true;
            this.lblComp.Location = new System.Drawing.Point(231, 20);
            this.lblComp.Name = "lblComp";
            this.lblComp.Size = new System.Drawing.Size(37, 13);
            this.lblComp.TabIndex = 12;
            this.lblComp.Text = "Comp.";
            // 
            // txtD6
            // 
            this.txtD6.Location = new System.Drawing.Point(196, 37);
            this.txtD6.Name = "txtD6";
            this.txtD6.Size = new System.Drawing.Size(32, 20);
            this.txtD6.TabIndex = 5;
            // 
            // lblD6
            // 
            this.lblD6.AutoSize = true;
            this.lblD6.Location = new System.Drawing.Point(193, 20);
            this.lblD6.Name = "lblD6";
            this.lblD6.Size = new System.Drawing.Size(20, 13);
            this.lblD6.TabIndex = 10;
            this.lblD6.Text = "#6";
            // 
            // txtD5
            // 
            this.txtD5.Location = new System.Drawing.Point(158, 37);
            this.txtD5.Name = "txtD5";
            this.txtD5.Size = new System.Drawing.Size(32, 20);
            this.txtD5.TabIndex = 4;
            // 
            // lblD5
            // 
            this.lblD5.AutoSize = true;
            this.lblD5.Location = new System.Drawing.Point(155, 20);
            this.lblD5.Name = "lblD5";
            this.lblD5.Size = new System.Drawing.Size(20, 13);
            this.lblD5.TabIndex = 8;
            this.lblD5.Text = "#5";
            // 
            // txtD4
            // 
            this.txtD4.Location = new System.Drawing.Point(120, 37);
            this.txtD4.Name = "txtD4";
            this.txtD4.Size = new System.Drawing.Size(32, 20);
            this.txtD4.TabIndex = 3;
            // 
            // lblD4
            // 
            this.lblD4.AutoSize = true;
            this.lblD4.Location = new System.Drawing.Point(117, 20);
            this.lblD4.Name = "lblD4";
            this.lblD4.Size = new System.Drawing.Size(20, 13);
            this.lblD4.TabIndex = 6;
            this.lblD4.Text = "#4";
            // 
            // txtD3
            // 
            this.txtD3.Location = new System.Drawing.Point(82, 37);
            this.txtD3.Name = "txtD3";
            this.txtD3.Size = new System.Drawing.Size(32, 20);
            this.txtD3.TabIndex = 2;
            // 
            // lblD3
            // 
            this.lblD3.AutoSize = true;
            this.lblD3.Location = new System.Drawing.Point(79, 20);
            this.lblD3.Name = "lblD3";
            this.lblD3.Size = new System.Drawing.Size(20, 13);
            this.lblD3.TabIndex = 4;
            this.lblD3.Text = "#3";
            // 
            // txtD2
            // 
            this.txtD2.Location = new System.Drawing.Point(44, 37);
            this.txtD2.Name = "txtD2";
            this.txtD2.Size = new System.Drawing.Size(32, 20);
            this.txtD2.TabIndex = 1;
            // 
            // lblD2
            // 
            this.lblD2.AutoSize = true;
            this.lblD2.Location = new System.Drawing.Point(41, 20);
            this.lblD2.Name = "lblD2";
            this.lblD2.Size = new System.Drawing.Size(20, 13);
            this.lblD2.TabIndex = 2;
            this.lblD2.Text = "#2";
            // 
            // txtD1
            // 
            this.txtD1.Location = new System.Drawing.Point(6, 37);
            this.txtD1.Name = "txtD1";
            this.txtD1.Size = new System.Drawing.Size(32, 20);
            this.txtD1.TabIndex = 0;
            // 
            // lblD1
            // 
            this.lblD1.AutoSize = true;
            this.lblD1.Location = new System.Drawing.Point(3, 20);
            this.lblD1.Name = "lblD1";
            this.lblD1.Size = new System.Drawing.Size(20, 13);
            this.lblD1.TabIndex = 0;
            this.lblD1.Text = "#1";
            // 
            // frm_Historico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(727, 408);
            this.Controls.Add(this.grbCombinacion);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.grbDatos);
            this.Name = "frm_Historico";
            this.Text = "frm_Historico";
            this.Load += new System.EventHandler(this.frm_Historico_Load);
            this.grbDatos.ResumeLayout(false);
            this.grbDatos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatos)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.grbCombinacion.ResumeLayout(false);
            this.grbCombinacion.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grbDatos;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.DataGridView dgvDatos;
        private System.Windows.Forms.ToolStripStatusLabel stslblInfo;
        private System.Windows.Forms.GroupBox grbCombinacion;
        private System.Windows.Forms.Label lblFechaSorteo;
        private System.Windows.Forms.DateTimePicker dtpFechaSorteo;
        private System.Windows.Forms.TextBox txtJoker;
        private System.Windows.Forms.Label lblJoker;
        private System.Windows.Forms.TextBox txtComplementario;
        private System.Windows.Forms.Label lblComp;
        private System.Windows.Forms.TextBox txtD6;
        private System.Windows.Forms.Label lblD6;
        private System.Windows.Forms.TextBox txtD5;
        private System.Windows.Forms.Label lblD5;
        private System.Windows.Forms.TextBox txtD4;
        private System.Windows.Forms.Label lblD4;
        private System.Windows.Forms.TextBox txtD3;
        private System.Windows.Forms.Label lblD3;
        private System.Windows.Forms.TextBox txtD2;
        private System.Windows.Forms.Label lblD2;
        private System.Windows.Forms.TextBox txtD1;
        private System.Windows.Forms.Label lblD1;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Label lblSorteo;
        private System.Windows.Forms.ComboBox cmbSorteo;
        private System.Windows.Forms.Button btnCargarHistorico;
        private System.Windows.Forms.TextBox txtBuscarD6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBuscarD5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBuscarD4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBuscarD3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBuscarD2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBuscarD1;
        private System.Windows.Forms.Label label6;
    }
}
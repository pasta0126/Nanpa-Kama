﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nanpa_Kama.View
{
    public partial class frm_Apuestas : Form
    {
        private Helper.Utility util = new Helper.Utility();
        private Manager.UI ui = new Manager.UI();
        private Manager.Datagridview dgvM = new Manager.Datagridview();
        private List<Helper.DgvHelper> lstColsGeneral;
        private List<Helper.DgvHelper> lstColsResultados;
        private Core.Combinaciones.Data data = Core.Combinaciones.Data.GetInstance();

        public frm_Apuestas()
        {
            InitializeComponent();
        }

        private void frm_Historico_Load(object sender, EventArgs e)
        {
            InitForm();
        }

        private void InitForm()
        {
            ui.FormStyler(this, "Apuestas");
            ui.StyleDG(new List<DataGridView> { dgvDatos, dgvResultados });

            CargarHandlers();

            lstColsGeneral = new List<Helper.DgvHelper>
            {
                new Helper.DgvHelper("id", "Id", 50, false, true),
                new Helper.DgvHelper("d1", "#1", 50, false, true),
                new Helper.DgvHelper("d2", "#2", 50, false, true),
                new Helper.DgvHelper("d3", "#3", 50, false, true),
                new Helper.DgvHelper("d4", "#4", 50, false, true),
                new Helper.DgvHelper("d5", "#5", 50, false, true),
                new Helper.DgvHelper("d6", "#6", 50, false, true),
                new Helper.DgvHelper("complementario", "comp.", 50, false, true),
                new Helper.DgvHelper("joker", "joker", 100, false, true),
                new Helper.DgvHelper("fecha_sorteo_1", "F. Sort. 1", 100, false, true),
                new Helper.DgvHelper("fecha_sorteo_2", "F. Sort. 2", 100, false, true),
                new Helper.DgvHelper("fecha_generado", "F. Generado", 100, false, true),
                new Helper.DgvHelper("comentario", "Comentario", 150, false, true),
                new Helper.DgvHelper("premiado", "Premiado", 50, false, true)
            };

            dgvDatos.DataSource = util.ToDataTable(data.lstCombinaciones);
            dgvM.OrderAndVisibleColumns(dgvDatos, lstColsGeneral);

            lstColsResultados = new List<Helper.DgvHelper>
            {
                new Helper.DgvHelper("Coincidencias", "Coincidencias", 100, false, true),
                new Helper.DgvHelper("#1", "#1", 50, false, true),
                new Helper.DgvHelper("#2", "#2", 50, false, true),
                new Helper.DgvHelper("#3", "#3", 50, false, true),
                new Helper.DgvHelper("#4", "#4", 50, false, true),
                new Helper.DgvHelper("#5", "#5", 50, false, true),
                new Helper.DgvHelper("#6", "#6", 50, false, true)
            };
        }

        private void CargarHandlers()
        {
            List<TextBox> lstTxtsBuscar = new List<TextBox> { txtBuscarD1, txtBuscarD2, txtBuscarD3, txtBuscarD4, txtBuscarD5, txtBuscarD6 };

            foreach (TextBox txtB in lstTxtsBuscar)
            {
                txtB.KeyUp += new KeyEventHandler(txt_KeyUpBuscar);
            }
        }

        private void txt_KeyUpBuscar(object sender, KeyEventArgs e)
        {
            Buscar();
        }

        private void CargarHistorico()
        {
            Cursor.Current = Cursors.WaitCursor;

            dgvDatos.DataSource = null;
            dgvDatos.Refresh();

            List<Model.t_combinaciones_historico> lstP = data.lstCombinacionesHistorico.OrderBy(o => o.fecha_sorteo).ToList();
            dgvDatos.DataSource = util.ToDataTable(lstP);
            dgvM.OrderAndVisibleColumns(dgvDatos, lstColsGeneral);

            stslblInfo.Text = string.Format("{0} registros cargados.", dgvDatos.Rows.Count);
            dgvDatos.FirstDisplayedScrollingRowIndex = dgvDatos.Rows.Count - 1;

            Cursor.Current = Cursors.Default;
        }

        private void Buscar()
        {
            List<Model.t_combinaciones> lstC = data.lstCombinaciones;

            if (dtpFechaSorteo1.Checked)
            {
                lstC = lstC.Where(w => w.fecha_sorteo_1 == Convert.ToDateTime(dtpFechaSorteo1.Value.ToString("yyyy-MM-dd"))).Select(s => s).ToList();
            }

            if (dtpFechaSorteo2.Checked)
            {
                lstC = lstC.Where(w => w.fecha_sorteo_2 == Convert.ToDateTime(dtpFechaSorteo2.Value.ToString("yyyy-MM-dd"))).Select(s => s).ToList();
            }

            for (int i = 1; i < 6; i++)
            {
                switch (i)
                {
                    case 1:
                        if (!string.IsNullOrEmpty(txtBuscarD1.Text))
                        {
                            lstC = lstC.Where(w => w.d1 == Convert.ToInt32(txtBuscarD1.Text)).Select(s => s).ToList();
                        }
                        break;

                    case 2:
                        if (!string.IsNullOrEmpty(txtBuscarD2.Text))
                        {
                            lstC = lstC.Where(w => w.d2 == Convert.ToInt32(txtBuscarD2.Text)).Select(s => s).ToList();
                        }
                        break;

                    case 3:
                        if (!string.IsNullOrEmpty(txtBuscarD3.Text))
                        {
                            lstC = lstC.Where(w => w.d3 == Convert.ToInt32(txtBuscarD3.Text)).Select(s => s).ToList();
                        }
                        break;

                    case 4:
                        if (!string.IsNullOrEmpty(txtBuscarD4.Text))
                        {
                            lstC = lstC.Where(w => w.d4 == Convert.ToInt32(txtBuscarD4.Text)).Select(s => s).ToList();
                        }
                        break;

                    case 5:
                        if (!string.IsNullOrEmpty(txtBuscarD5.Text))
                        {
                            lstC = lstC.Where(w => w.d5 == Convert.ToInt32(txtBuscarD5.Text)).Select(s => s).ToList();
                        }
                        break;

                    case 6:
                        if (!string.IsNullOrEmpty(txtBuscarD6.Text))
                        {
                            lstC = lstC.Where(w => w.d6 == Convert.ToInt32(txtBuscarD6.Text)).Select(s => s).ToList();
                        }
                        break;

                    default:
                        break;
                }
            }

            dgvDatos.DataSource = util.ToDataTable(lstC);
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtBuscarD1.Text = string.Empty;
            txtBuscarD2.Text = string.Empty;
            txtBuscarD3.Text = string.Empty;
            txtBuscarD4.Text = string.Empty;
            txtBuscarD5.Text = string.Empty;
            txtBuscarD6.Text = string.Empty;

            dtpFechaSorteo1.Checked = false;
            dtpFechaSorteo2.Checked = false;

            dgvDatos.DataSource = util.ToDataTable(data.lstCombinaciones);
        }

        private void txtCombinacionGanadora_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DataTable dt = new DataTable();

                dt.Columns.Add("Coincidencias");
                dt.Columns.Add("#1");
                dt.Columns.Add("#2");
                dt.Columns.Add("#3");
                dt.Columns.Add("#4");
                dt.Columns.Add("#5");
                dt.Columns.Add("#6");

                List<Model.t_combinaciones> lst = new List<Model.t_combinaciones> { };
                List<Model.t_combinaciones> lstC = data.lstCombinaciones;

                if (dtpFechaSorteo1.Checked)
                {
                    lstC = lstC.Where(w => w.fecha_sorteo_1 == Convert.ToDateTime(dtpFechaSorteo1.Value.ToString("yyyy-MM-dd"))).Select(s => s).ToList();
                }

                if (dtpFechaSorteo2.Checked)
                {
                    lstC = lstC.Where(w => w.fecha_sorteo_2 == Convert.ToDateTime(dtpFechaSorteo2.Value.ToString("yyyy-MM-dd"))).Select(s => s).ToList();
                }

                if (!string.IsNullOrEmpty(txtCombinacionGanadora.Text))
                {
                    string[] arr = txtCombinacionGanadora.Text.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (Model.t_combinaciones comb in lstC)
                    {
                        int coincidencias = 0;

                        for (int i = 0; i < arr.Length - 1; i++)
                        {
                            int a = Convert.ToInt32(arr[i]);

                            if (comb.d1 == a)
                            {
                                coincidencias += 1;
                            }

                            if (comb.d2 == a)
                            {
                                coincidencias += 1;
                            }

                            if (comb.d3 == a)
                            {
                                coincidencias += 1;
                            }

                            if (comb.d4 == a)
                            {
                                coincidencias += 1;
                            }

                            if (comb.d5 == a)
                            {
                                coincidencias += 1;
                            }

                            if (comb.d6 == a)
                            {
                                coincidencias += 1;
                            }
                        }

                        if (coincidencias > 0)
                        {
                            dt.Rows.Add(coincidencias, comb.d1, comb.d2, comb.d3, comb.d4, comb.d5, comb.d6);
                        }
                    }
                }

                dgvResultados.DataSource = dt;
                dgvM.OrderAndVisibleColumns(dgvResultados, lstColsResultados);
            }
        }
    }
}

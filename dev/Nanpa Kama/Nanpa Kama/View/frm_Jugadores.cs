﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nanpa_Kama.View
{
    public partial class frm_Jugadores : Form
    {
        private Helper.Utility util = new Helper.Utility();
        private Manager.UI ui = new Manager.UI();
        private Manager.Datagridview dgvM = new Manager.Datagridview();
        private List<Helper.DgvHelper> lstColsJugadores;

        List<Model.t_master_jugadores> lstJugadores = new List<Model.t_master_jugadores> { };

        public frm_Jugadores()
        {
            InitializeComponent();
        }

        private void frm_Jugadores_Load(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            InitForm();

            Cursor.Current = Cursors.Default;
        }

        private void InitForm()
        {
            ui.FormStyler(this, "Jugadores");
            ui.StyleDG(new List<DataGridView> { dgvLista });

            using (Model.Nanpa_Kama_Model context = new Model.Nanpa_Kama_Model())
            {
                lstJugadores = context.t_master_jugadores.Where(w => w.fecha_baja == null).Select(s => s).ToList();
            }

            lstColsJugadores = new List<Helper.DgvHelper>
            {
                new Helper.DgvHelper("id", "Id", 50, false, true),
                new Helper.DgvHelper("jugador", "Jugador", 250, false, true),
                new Helper.DgvHelper("participacion_euros_sorteo", "Part. €/sort.", 150, true, true),
                new Helper.DgvHelper("fecha_alta", "Alta", 100, false, true),
                new Helper.DgvHelper("fecha_baja", "Baja", 100, false, true)
            };

            LoadJugadores();
        }

        private void LoadJugadores()
        {
            dgvLista.DataSource = util.ToDataTable(lstJugadores);
            dgvM.OrderAndVisibleColumns(dgvLista, lstColsJugadores);
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (ValidarNewJugador())
            {
                Model.t_master_jugadores jugador = new Model.t_master_jugadores();

                Cursor.Current = Cursors.WaitCursor;

                jugador.jugador = txtJugador.Text;
                jugador.participacion_euros_sorteo = Convert.ToInt32(txtParticipacion.Text);
                jugador.fecha_alta = Convert.ToDateTime(dtpFechaAlta.Text);

                using (Model.Nanpa_Kama_Model context = new Model.Nanpa_Kama_Model())
                {
                    context.t_master_jugadores.Add(jugador);
                    context.SaveChanges();
                }

                lstJugadores.Add(jugador);

                LoadJugadores();
                LimpiarForm();

                Cursor.Current = Cursors.Default;
            }
            else
            {
                string text = "Faltan datos obligatorios.";
                string title = Application.ProductName;
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBoxIcon icon = MessageBoxIcon.Exclamation;
                MessageBox.Show(text, title, buttons, icon);
            }
        }

        private bool ValidarNewJugador()
        {
            bool result = false;

            if (!string.IsNullOrEmpty(txtJugador.Text))
            {
                if (!string.IsNullOrEmpty(txtParticipacion.Text) && Int32.Parse(txtParticipacion.Text.Trim()) > 0)
                {
                    result = true;
                }
            }

            return result;
        }

        private void LimpiarForm()
        {
            txtJugador.Text = string.Empty;
            txtParticipacion.Text = string.Empty;
            dtpFechaAlta.Value = DateTime.Now;
        }

        private void btnBaja_Click(object sender, EventArgs e)
        {
            string text = "Estás seguro/a de querer dar de baja estos jugadores?";
            string title = Application.ProductName;
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            MessageBoxIcon icon = MessageBoxIcon.Question;
            MessageBoxDefaultButton defaultbuttons = MessageBoxDefaultButton.Button2;
            DialogResult dialogResult = MessageBox.Show(text, title, buttons, icon, defaultbuttons);

            if (dialogResult == DialogResult.Yes)
            {
                Cursor.Current = Cursors.WaitCursor;

                using (Model.Nanpa_Kama_Model context = new Model.Nanpa_Kama_Model())
                {
                    foreach (DataGridViewRow row in dgvLista.SelectedRows)
                    {
                        int id = Convert.ToInt32(row.Cells["id"].Value);
                        Model.t_master_jugadores jugador = context.t_master_jugadores.Where(w => w.id == id).Select(s => s).Single();
                        Model.t_master_jugadores j = lstJugadores.Where(w => w.id == id).Select(s => s).Single();

                        lstJugadores.Remove(j);
                        jugador.fecha_baja = DateTime.Now;
                    }

                    context.SaveChanges();
                }

                LoadJugadores();

                Cursor.Current = Cursors.Default;
            }
        }
    }
}

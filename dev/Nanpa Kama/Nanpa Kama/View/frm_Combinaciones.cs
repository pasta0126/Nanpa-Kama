﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Nanpa_Kama.Model.Data;
using System.Data;
using System.Diagnostics;

namespace Nanpa_Kama.View
{
    public partial class frm_Combinaciones : Form
    {
        private Helper.Utility util = new Helper.Utility();
        private Manager.UI ui = new Manager.UI();
        private List<Model.Data.Combinacion> lstCombinaciones = new List<Model.Data.Combinacion> { };

        private bool guardado = true;
        private string filtroDescartador = string.Empty;

        private Manager.Datagridview dgvM = new Manager.Datagridview();
        private List<Helper.DgvHelper> lstColsCombinaciones;

        public frm_Combinaciones()
        {
            InitializeComponent();
        }

        private void frm_Combinaciones_Load(object sender, EventArgs e)
        {
            InitForm();
        }

        private void InitForm()
        {
            ui.FormStyler(this, "Combinaciones");
            ui.StyleDG(new List<DataGridView> { dgvLista });
            txtCombinacions.Text = (Core.Valores.Mode == Core.Valores.Modo.Desarrollo) ? "10" : "100";

            lstColsCombinaciones = new List<Helper.DgvHelper>
            {
                //new Helper.DgvHelper("IsValid", "Válido", 50, true, false),
                new Helper.DgvHelper("d1", "#1", 50, false, true),
                new Helper.DgvHelper("d2", "#2", 50, false, true),
                new Helper.DgvHelper("d3", "#3", 50, false, true),
                new Helper.DgvHelper("d4", "#4", 50, false, true),
                new Helper.DgvHelper("d5", "#5", 50, false, true),
                new Helper.DgvHelper("d6", "#6", 50, false, true),
                new Helper.DgvHelper("complementario", "comp.", 50, false, true),
                new Helper.DgvHelper("joker", "joker", 50, false, true)
            };

            dgvLista.DataSource = util.ToDataTable(lstCombinaciones);
            dgvM.OrderAndVisibleColumns(dgvLista, lstColsCombinaciones);
        }

        private void GenerarCombinaciones()
        {
            Helper.Log log = new Helper.Log(txtSucesos);
            Manager.Progress progress = new Manager.Progress(prgProgreso, txtProgressInfo);
            int maxCombinaciones = Convert.ToInt32(txtCombinacions.Text);
            lstCombinaciones.Clear();

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            while (lstCombinaciones.Count < maxCombinaciones)
            {
                Core.Combinaciones.Generar generar = new Core.Combinaciones.Generar();
                Model.Data.Combinacion combinacion = generar.combinacion;

                Core.Combinaciones.Validar validar = new Core.Combinaciones.Validar();
                bool agregarCombinacion = false;

                if (Core.Valores.Mode != Core.Valores.Modo.Desarrollo)
                {
                    if (validar.Lista(combinacion, lstCombinaciones))
                    {
                        if (validar.Historico(combinacion))
                        {
                            if (ValidarReglas(combinacion))
                            {
                                agregarCombinacion = true;
                            }
                            else
                            {
                                log.Add(string.Format("{0}|{1}|{2}", combinacion.Cadena(), 0, Core.Valores.LogConceptos.DescartadoFiltros + ": " + filtroDescartador));
                            }
                        }
                        else
                        {
                            log.Add(string.Format("{0}|{1}|{2}", combinacion.Cadena(), validar.ElapsedTimeHistorico, Core.Valores.LogConceptos.DescartadoHistorico));
                        }
                    }
                    else
                    {
                        log.Add(string.Format("{0}|{1}|{2}", combinacion.Cadena(), validar.ElapsedTimeLista, Core.Valores.LogConceptos.DescartadoLista));
                    }
                }
                else
                {
                    log.Add(string.Format("{0}|{1}|{2}", combinacion.Cadena(), string.Empty, Core.Valores.LogConceptos.SinValidacion));
                    agregarCombinacion = true;
                }

                if (agregarCombinacion)
                {
                    lstCombinaciones.Add(combinacion);

                    int porcentage = util.Porcentage(lstCombinaciones.Count, maxCombinaciones);
                    string status = string.Format("{0}/{1} generados {2}% completado", lstCombinaciones.Count, maxCombinaciones, porcentage);

                    progress.UpdateProgress(status, porcentage);
                }
            }

            stopWatch.Stop();
            TimeSpan time = stopWatch.Elapsed;

            if (lstCombinaciones.Count >= Convert.ToInt32(txtCombinacions.Text) || lstCombinaciones.Count == 0)
            {
                string message = "Finalizado ... ";
                message += string.Format("{0:00}:{1:00}:{2:00}.{3:00}", time.Hours, time.Minutes, time.Seconds, time.Milliseconds / 10);

                log.Add(message);

                progress.UpdateProgress(message, 100);
                guardado = false;
            }
        }

        private bool ValidarReglas(Model.Data.Combinacion comb)
        {
            bool result = true;
            Core.Combinaciones.Filtros filtro = new Core.Combinaciones.Filtros();

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            filtroDescartador = string.Empty;

            // par
            if (result && filtro.ParImpar(comb, 4, true))
            {
                filtroDescartador = Core.Valores.LogConceptos.FiltrolaParImpar + "(Par)";
                result = false;
            }

            //// impar
            //if (result && filtro.ParImpar(comb, 4, false))
            //{
            //    filtroDescartador = Core.Valores.LogConceptos.FiltrolaParImpar + "(Impar)";
            //    result = false;
            //}

            //// seguidos
            //for (int i = 1; i <= 10; i++)
            //{
            //    if (result && filtro.Seguidos(comb, 2, i))
            //    {
            //        filtroDescartador = Core.Valores.LogConceptos.FiltroSeguidos.ToString();
            //        result = false;
            //        break;
            //    }
            //}

            stopWatch.Stop();
            TimeSpan time = stopWatch.Elapsed;

            return result;
        }

        private void Generar()
        {
            string text = "Estás seguro/a de querer generar las combinaciones?";
            string title = Application.ProductName;
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            MessageBoxIcon icon = MessageBoxIcon.Question;
            MessageBoxDefaultButton defaultbuttons = MessageBoxDefaultButton.Button2;
            DialogResult dialogResult = MessageBox.Show(text, title, buttons, icon, defaultbuttons);

            if (dialogResult == DialogResult.Yes)
            {
                if (!guardado)
                {
                    text = "Hay combinaciones pendientes de guardar \n Quieres guardalas?";
                    dialogResult = MessageBox.Show(text, title, buttons, icon, defaultbuttons);

                    if (dialogResult == DialogResult.Yes)
                    {
                        GuardarCombinaciones(lstCombinaciones);
                    }
                }

                Cursor.Current = Cursors.WaitCursor;

                stslblInfo.Text = string.Empty;
                stsStatus.Refresh();

                dgvLista.DataSource = null;
                dgvLista.Refresh();
                txtProgressInfo.Text = string.Empty;
                txtProgressInfo.Refresh();

                GenerarCombinaciones();

                dgvLista.DataSource = util.ToDataTable(lstCombinaciones);
                dgvM.OrderAndVisibleColumns(dgvLista, lstColsCombinaciones);

                stslblInfo.Text = string.Format("Generado: {0} combinaciones", txtCombinacions.Text);

                Cursor.Current = Cursors.Default;
            }
        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            Generar();
        }

        private void GuardarCombinaciones(List<Combinacion> lst)
        {
            GuardarCombinaciones(lstCombinaciones, true);
        }

        private void GuardarCombinaciones(List<Combinacion> lst, bool withMessage)
        {
            if (lst.Count > 0)
            {
                using (Model.Nanpa_Kama_Model context = new Model.Nanpa_Kama_Model())
                {
                    foreach (Combinacion comb in lst)
                    {
                        Core.Combinaciones.Acciones accion = new Core.Combinaciones.Acciones();
                        Model.t_combinaciones combinacion = accion.Convert(comb);

                        context.t_combinaciones.Add(combinacion);
                    }

                    context.SaveChanges();
                    guardado = true;
                }
            }

            if (withMessage)
            {
                string text = string.Format("Se han guardado las {0} combinaciones en la BBDD.", lstCombinaciones.Count);
                string title = Application.ProductName;
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBoxIcon icon = MessageBoxIcon.Information;
                MessageBox.Show(text, title, buttons, icon);
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (!guardado)
            {
                GuardarCombinaciones(lstCombinaciones);
            }
            else
            {
                string text = "Esta combinación ya ha sido guardada. \n Estás seguro/a de querer guardarla de nuevo?";
                string title = Application.ProductName;
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                MessageBoxIcon icon = MessageBoxIcon.Question;
                MessageBoxDefaultButton defaultbuttons = MessageBoxDefaultButton.Button2;
                DialogResult dialogResult = MessageBox.Show(text, title, buttons, icon, defaultbuttons);

                if (dialogResult == DialogResult.Yes)
                {
                    GuardarCombinaciones(lstCombinaciones);
                }
            }
        }

        private void txtCombinacions_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Generar();
            }
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            GuardarCombinaciones(lstCombinaciones, false);

            Helper.Export export = new Helper.Export();
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "Excel xlsx|*.xlsx";
            saveFileDialog1.Title = "Guardar combinaciones";
            saveFileDialog1.ShowDialog();

            if (export.ToExcel((DataTable)dgvLista.DataSource, saveFileDialog1.FileName))
            {
                string text = "Se ha guardado correctamente el fichero. \n" + saveFileDialog1.FileName;

                MessageBox.Show(text, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                stslblInfo.Text = string.Format("Fichero guardado en: " + saveFileDialog1.FileName);
            }
        }
    }
}

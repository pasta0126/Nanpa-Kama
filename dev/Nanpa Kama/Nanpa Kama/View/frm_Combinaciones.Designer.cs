﻿namespace Nanpa_Kama.View
{
    partial class frm_Combinaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGenerar = new System.Windows.Forms.Button();
            this.txtCombinacions = new System.Windows.Forms.TextBox();
            this.grbAcciones = new System.Windows.Forms.GroupBox();
            this.txtProgressInfo = new System.Windows.Forms.TextBox();
            this.prgProgreso = new System.Windows.Forms.ProgressBar();
            this.grbLista = new System.Windows.Forms.GroupBox();
            this.dgvLista = new System.Windows.Forms.DataGridView();
            this.stsStatus = new System.Windows.Forms.StatusStrip();
            this.stslblInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnExportar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.grbSucesos = new System.Windows.Forms.GroupBox();
            this.txtSucesos = new System.Windows.Forms.TextBox();
            this.grbAcciones.SuspendLayout();
            this.grbLista.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).BeginInit();
            this.stsStatus.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grbSucesos.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGenerar
            // 
            this.btnGenerar.Location = new System.Drawing.Point(6, 18);
            this.btnGenerar.Name = "btnGenerar";
            this.btnGenerar.Size = new System.Drawing.Size(146, 23);
            this.btnGenerar.TabIndex = 0;
            this.btnGenerar.Text = "Generar Combinaciones";
            this.btnGenerar.UseVisualStyleBackColor = true;
            this.btnGenerar.Click += new System.EventHandler(this.btnGenerar_Click);
            // 
            // txtCombinacions
            // 
            this.txtCombinacions.Location = new System.Drawing.Point(158, 19);
            this.txtCombinacions.Name = "txtCombinacions";
            this.txtCombinacions.Size = new System.Drawing.Size(50, 20);
            this.txtCombinacions.TabIndex = 1;
            this.txtCombinacions.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCombinacions_KeyDown);
            // 
            // grbAcciones
            // 
            this.grbAcciones.Controls.Add(this.txtProgressInfo);
            this.grbAcciones.Controls.Add(this.prgProgreso);
            this.grbAcciones.Controls.Add(this.btnGenerar);
            this.grbAcciones.Controls.Add(this.txtCombinacions);
            this.grbAcciones.Location = new System.Drawing.Point(12, 12);
            this.grbAcciones.Name = "grbAcciones";
            this.grbAcciones.Size = new System.Drawing.Size(285, 79);
            this.grbAcciones.TabIndex = 3;
            this.grbAcciones.TabStop = false;
            // 
            // txtProgressInfo
            // 
            this.txtProgressInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProgressInfo.BackColor = System.Drawing.SystemColors.Control;
            this.txtProgressInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtProgressInfo.Location = new System.Drawing.Point(6, 55);
            this.txtProgressInfo.Name = "txtProgressInfo";
            this.txtProgressInfo.Size = new System.Drawing.Size(273, 13);
            this.txtProgressInfo.TabIndex = 4;
            this.txtProgressInfo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // prgProgreso
            // 
            this.prgProgreso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.prgProgreso.Location = new System.Drawing.Point(6, 48);
            this.prgProgreso.MarqueeAnimationSpeed = 10;
            this.prgProgreso.Name = "prgProgreso";
            this.prgProgreso.Size = new System.Drawing.Size(273, 1);
            this.prgProgreso.TabIndex = 3;
            // 
            // grbLista
            // 
            this.grbLista.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.grbLista.Controls.Add(this.dgvLista);
            this.grbLista.Location = new System.Drawing.Point(303, 12);
            this.grbLista.Name = "grbLista";
            this.grbLista.Size = new System.Drawing.Size(506, 378);
            this.grbLista.TabIndex = 4;
            this.grbLista.TabStop = false;
            // 
            // dgvLista
            // 
            this.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLista.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLista.Location = new System.Drawing.Point(3, 16);
            this.dgvLista.Name = "dgvLista";
            this.dgvLista.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvLista.Size = new System.Drawing.Size(500, 359);
            this.dgvLista.TabIndex = 0;
            // 
            // stsStatus
            // 
            this.stsStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stslblInfo});
            this.stsStatus.Location = new System.Drawing.Point(0, 393);
            this.stsStatus.Name = "stsStatus";
            this.stsStatus.Size = new System.Drawing.Size(1226, 22);
            this.stsStatus.TabIndex = 5;
            this.stsStatus.Text = "statusStrip1";
            // 
            // stslblInfo
            // 
            this.stslblInfo.Name = "stslblInfo";
            this.stslblInfo.Size = new System.Drawing.Size(10, 17);
            this.stslblInfo.Text = " ";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.btnExportar);
            this.groupBox1.Controls.Add(this.btnGuardar);
            this.groupBox1.Location = new System.Drawing.Point(12, 97);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(285, 293);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // btnExportar
            // 
            this.btnExportar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExportar.Location = new System.Drawing.Point(186, 264);
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Size = new System.Drawing.Size(69, 23);
            this.btnExportar.TabIndex = 2;
            this.btnExportar.Text = "Exportar";
            this.btnExportar.UseVisualStyleBackColor = true;
            this.btnExportar.Click += new System.EventHandler(this.btnExportar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGuardar.Location = new System.Drawing.Point(111, 264);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(69, 23);
            this.btnGuardar.TabIndex = 1;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // grbSucesos
            // 
            this.grbSucesos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbSucesos.Controls.Add(this.txtSucesos);
            this.grbSucesos.Location = new System.Drawing.Point(815, 12);
            this.grbSucesos.Name = "grbSucesos";
            this.grbSucesos.Size = new System.Drawing.Size(399, 378);
            this.grbSucesos.TabIndex = 7;
            this.grbSucesos.TabStop = false;
            // 
            // txtSucesos
            // 
            this.txtSucesos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSucesos.Location = new System.Drawing.Point(3, 16);
            this.txtSucesos.Multiline = true;
            this.txtSucesos.Name = "txtSucesos";
            this.txtSucesos.ReadOnly = true;
            this.txtSucesos.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtSucesos.Size = new System.Drawing.Size(393, 359);
            this.txtSucesos.TabIndex = 0;
            // 
            // frm_Combinaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1226, 415);
            this.Controls.Add(this.grbSucesos);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.stsStatus);
            this.Controls.Add(this.grbLista);
            this.Controls.Add(this.grbAcciones);
            this.Name = "frm_Combinaciones";
            this.Text = "frm_Combinaciones";
            this.Load += new System.EventHandler(this.frm_Combinaciones_Load);
            this.grbAcciones.ResumeLayout(false);
            this.grbAcciones.PerformLayout();
            this.grbLista.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).EndInit();
            this.stsStatus.ResumeLayout(false);
            this.stsStatus.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.grbSucesos.ResumeLayout(false);
            this.grbSucesos.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGenerar;
        private System.Windows.Forms.TextBox txtCombinacions;
        private System.Windows.Forms.GroupBox grbAcciones;
        private System.Windows.Forms.GroupBox grbLista;
        private System.Windows.Forms.DataGridView dgvLista;
        private System.Windows.Forms.StatusStrip stsStatus;
        private System.Windows.Forms.ToolStripStatusLabel stslblInfo;
        private System.Windows.Forms.ProgressBar prgProgreso;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.TextBox txtProgressInfo;
        private System.Windows.Forms.Button btnExportar;
        private System.Windows.Forms.GroupBox grbSucesos;
        private System.Windows.Forms.TextBox txtSucesos;
    }
}
﻿namespace Nanpa_Kama.View
{
    partial class frm_Apuestas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grbDatos = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCombinacionGanadora = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpFechaSorteo2 = new System.Windows.Forms.DateTimePicker();
            this.lblFechaSorteo = new System.Windows.Forms.Label();
            this.dtpFechaSorteo1 = new System.Windows.Forms.DateTimePicker();
            this.txtBuscarD6 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBuscarD5 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBuscarD4 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBuscarD3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBuscarD2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBuscarD1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.dgvDatos = new System.Windows.Forms.DataGridView();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.stslblInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvResultados = new System.Windows.Forms.DataGridView();
            this.grbDatos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatos)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResultados)).BeginInit();
            this.SuspendLayout();
            // 
            // grbDatos
            // 
            this.grbDatos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbDatos.Controls.Add(this.label7);
            this.grbDatos.Controls.Add(this.dtpFechaSorteo2);
            this.grbDatos.Controls.Add(this.lblFechaSorteo);
            this.grbDatos.Controls.Add(this.dtpFechaSorteo1);
            this.grbDatos.Controls.Add(this.txtBuscarD6);
            this.grbDatos.Controls.Add(this.label1);
            this.grbDatos.Controls.Add(this.txtBuscarD5);
            this.grbDatos.Controls.Add(this.label2);
            this.grbDatos.Controls.Add(this.txtBuscarD4);
            this.grbDatos.Controls.Add(this.label3);
            this.grbDatos.Controls.Add(this.txtBuscarD3);
            this.grbDatos.Controls.Add(this.label4);
            this.grbDatos.Controls.Add(this.txtBuscarD2);
            this.grbDatos.Controls.Add(this.label5);
            this.grbDatos.Controls.Add(this.txtBuscarD1);
            this.grbDatos.Controls.Add(this.label6);
            this.grbDatos.Controls.Add(this.btnLimpiar);
            this.grbDatos.Controls.Add(this.dgvDatos);
            this.grbDatos.Location = new System.Drawing.Point(12, 12);
            this.grbDatos.Name = "grbDatos";
            this.grbDatos.Size = new System.Drawing.Size(593, 371);
            this.grbDatos.TabIndex = 1;
            this.grbDatos.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 13);
            this.label8.TabIndex = 29;
            this.label8.Text = "Comb. Ganadora";
            // 
            // txtCombinacionGanadora
            // 
            this.txtCombinacionGanadora.Location = new System.Drawing.Point(6, 31);
            this.txtCombinacionGanadora.Name = "txtCombinacionGanadora";
            this.txtCombinacionGanadora.Size = new System.Drawing.Size(161, 20);
            this.txtCombinacionGanadora.TabIndex = 28;
            this.txtCombinacionGanadora.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtCombinacionGanadora_KeyUp);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(451, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "Fecha sorteo 2";
            // 
            // dtpFechaSorteo2
            // 
            this.dtpFechaSorteo2.Checked = false;
            this.dtpFechaSorteo2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaSorteo2.Location = new System.Drawing.Point(454, 31);
            this.dtpFechaSorteo2.Name = "dtpFechaSorteo2";
            this.dtpFechaSorteo2.ShowCheckBox = true;
            this.dtpFechaSorteo2.Size = new System.Drawing.Size(100, 20);
            this.dtpFechaSorteo2.TabIndex = 26;
            // 
            // lblFechaSorteo
            // 
            this.lblFechaSorteo.AutoSize = true;
            this.lblFechaSorteo.Location = new System.Drawing.Point(345, 14);
            this.lblFechaSorteo.Name = "lblFechaSorteo";
            this.lblFechaSorteo.Size = new System.Drawing.Size(78, 13);
            this.lblFechaSorteo.TabIndex = 25;
            this.lblFechaSorteo.Text = "Fecha sorteo 1";
            // 
            // dtpFechaSorteo1
            // 
            this.dtpFechaSorteo1.Checked = false;
            this.dtpFechaSorteo1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaSorteo1.Location = new System.Drawing.Point(348, 31);
            this.dtpFechaSorteo1.Name = "dtpFechaSorteo1";
            this.dtpFechaSorteo1.ShowCheckBox = true;
            this.dtpFechaSorteo1.Size = new System.Drawing.Size(100, 20);
            this.dtpFechaSorteo1.TabIndex = 24;
            // 
            // txtBuscarD6
            // 
            this.txtBuscarD6.Location = new System.Drawing.Point(310, 30);
            this.txtBuscarD6.Name = "txtBuscarD6";
            this.txtBuscarD6.Size = new System.Drawing.Size(32, 20);
            this.txtBuscarD6.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(307, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "#6";
            // 
            // txtBuscarD5
            // 
            this.txtBuscarD5.Location = new System.Drawing.Point(272, 30);
            this.txtBuscarD5.Name = "txtBuscarD5";
            this.txtBuscarD5.Size = new System.Drawing.Size(32, 20);
            this.txtBuscarD5.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(269, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "#5";
            // 
            // txtBuscarD4
            // 
            this.txtBuscarD4.Location = new System.Drawing.Point(234, 30);
            this.txtBuscarD4.Name = "txtBuscarD4";
            this.txtBuscarD4.Size = new System.Drawing.Size(32, 20);
            this.txtBuscarD4.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(231, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "#4";
            // 
            // txtBuscarD3
            // 
            this.txtBuscarD3.Location = new System.Drawing.Point(196, 30);
            this.txtBuscarD3.Name = "txtBuscarD3";
            this.txtBuscarD3.Size = new System.Drawing.Size(32, 20);
            this.txtBuscarD3.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(193, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "#3";
            // 
            // txtBuscarD2
            // 
            this.txtBuscarD2.Location = new System.Drawing.Point(158, 30);
            this.txtBuscarD2.Name = "txtBuscarD2";
            this.txtBuscarD2.Size = new System.Drawing.Size(32, 20);
            this.txtBuscarD2.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(155, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "#2";
            // 
            // txtBuscarD1
            // 
            this.txtBuscarD1.Location = new System.Drawing.Point(120, 30);
            this.txtBuscarD1.Name = "txtBuscarD1";
            this.txtBuscarD1.Size = new System.Drawing.Size(32, 20);
            this.txtBuscarD1.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(117, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "#1";
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(6, 28);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(108, 23);
            this.btnLimpiar.TabIndex = 11;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // dgvDatos
            // 
            this.dgvDatos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatos.Location = new System.Drawing.Point(6, 57);
            this.dgvDatos.Name = "dgvDatos";
            this.dgvDatos.Size = new System.Drawing.Size(581, 308);
            this.dgvDatos.TabIndex = 1;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stslblInfo});
            this.statusStrip1.Location = new System.Drawing.Point(0, 386);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1096, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // stslblInfo
            // 
            this.stslblInfo.Name = "stslblInfo";
            this.stslblInfo.Size = new System.Drawing.Size(10, 17);
            this.stslblInfo.Text = " ";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.dgvResultados);
            this.groupBox1.Controls.Add(this.txtCombinacionGanadora);
            this.groupBox1.Location = new System.Drawing.Point(611, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(473, 371);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // dgvResultados
            // 
            this.dgvResultados.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvResultados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResultados.Location = new System.Drawing.Point(6, 57);
            this.dgvResultados.Name = "dgvResultados";
            this.dgvResultados.Size = new System.Drawing.Size(461, 308);
            this.dgvResultados.TabIndex = 1;
            // 
            // frm_Apuestas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1096, 408);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.grbDatos);
            this.Name = "frm_Apuestas";
            this.Text = "frm_Historico";
            this.Load += new System.EventHandler(this.frm_Historico_Load);
            this.grbDatos.ResumeLayout(false);
            this.grbDatos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatos)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResultados)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grbDatos;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.DataGridView dgvDatos;
        private System.Windows.Forms.ToolStripStatusLabel stslblInfo;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.TextBox txtBuscarD6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBuscarD5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBuscarD4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBuscarD3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBuscarD2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBuscarD1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtpFechaSorteo2;
        private System.Windows.Forms.Label lblFechaSorteo;
        private System.Windows.Forms.DateTimePicker dtpFechaSorteo1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCombinacionGanadora;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvResultados;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nanpa_Kama.View
{
    public partial class frm_Historico : Form
    {
        private Helper.Utility util = new Helper.Utility();
        private Manager.UI ui = new Manager.UI();
        private Manager.Datagridview dgvM = new Manager.Datagridview();
        private List<Helper.DgvHelper> lstColsGeneral;
        private Core.Combinaciones.Data data = Core.Combinaciones.Data.GetInstance();

        public frm_Historico()
        {
            InitializeComponent();
        }

        private void frm_Historico_Load(object sender, EventArgs e)
        {
            InitForm();
        }

        private void InitForm()
        {
            ui.FormStyler(this, "Histórico");
            ui.StyleDG(new List<DataGridView> { dgvDatos });

            CargarHandlers();

            lstColsGeneral = new List<Helper.DgvHelper>
            {
                new Helper.DgvHelper("d1", "#1", 50, true, true),
                new Helper.DgvHelper("d2", "#2", 50, true, true),
                new Helper.DgvHelper("d3", "#3", 50, true, true),
                new Helper.DgvHelper("d4", "#4", 50, true, true),
                new Helper.DgvHelper("d5", "#5", 50, true, true),
                new Helper.DgvHelper("d6", "#6", 50, true, true),
                new Helper.DgvHelper("complementario", "comp.", 50, true, true),
                new Helper.DgvHelper("joker", "joker", 100, true, true),
                new Helper.DgvHelper("fecha_sorteo", "F. Sorteo", 150, true, true)
            };

            dgvDatos.DataSource = util.ToDataTable(data.lstCombinacionesHistorico);
            dgvM.OrderAndVisibleColumns(dgvDatos, lstColsGeneral);
        }

        private void CargarHandlers()
        {
            List<TextBox> lstTxts = new List<TextBox> { txtD1, txtD2, txtD3, txtD4, txtD5, txtD6, txtComplementario, txtJoker };

            foreach (TextBox txt in lstTxts)
            {
                txt.KeyDown += new KeyEventHandler(txt_KeyDown);
            }

            List<TextBox> lstTxtsBuscar = new List<TextBox> { txtBuscarD1, txtBuscarD2, txtBuscarD3, txtBuscarD4, txtBuscarD5, txtBuscarD6 };

            foreach (TextBox txtB in lstTxtsBuscar)
            {
                txtB.KeyUp += new KeyEventHandler(txt_KeyUpBuscar);
            }
        }

        private void txt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Guardar();
            }
        }

        private void txt_KeyUpBuscar(object sender, KeyEventArgs e)
        {
            Buscar();
        }

        private void CargarHistorico()
        {
            Cursor.Current = Cursors.WaitCursor;

            dgvDatos.DataSource = null;
            dgvDatos.Refresh();

            List<Model.t_combinaciones_historico> lstP = data.lstCombinacionesHistorico.OrderBy(o => o.fecha_sorteo).ToList();
            dgvDatos.DataSource = util.ToDataTable(lstP);
            dgvM.OrderAndVisibleColumns(dgvDatos, lstColsGeneral);

            stslblInfo.Text = string.Format("{0} registros cargados.", dgvDatos.Rows.Count);
            dgvDatos.FirstDisplayedScrollingRowIndex = dgvDatos.Rows.Count - 1;

            Cursor.Current = Cursors.Default;
        }

        private void cmbTipoSorteo_SelectedValueChanged(object sender, EventArgs e)
        {
            CargarHistorico();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        private void Guardar()
        {
            if (!string.IsNullOrEmpty(txtD1.Text) &&
                !string.IsNullOrEmpty(txtD2.Text) &&
                !string.IsNullOrEmpty(txtD3.Text) &&
                !string.IsNullOrEmpty(txtD4.Text) &&
                !string.IsNullOrEmpty(txtD5.Text) &&
                !string.IsNullOrEmpty(txtD6.Text) &&
                dtpFechaSorteo.Value != null &&
                cmbSorteo.SelectedIndex != -1)
            {

                Model.t_combinaciones_historico p = new Model.t_combinaciones_historico();

                p.complementario = Convert.ToInt32(txtComplementario.Text);
                p.d1 = Convert.ToInt32(txtD1.Text);
                p.d2 = Convert.ToInt32(txtD2.Text);
                p.d3 = Convert.ToInt32(txtD3.Text);
                p.d4 = Convert.ToInt32(txtD4.Text);
                p.d5 = Convert.ToInt32(txtD5.Text);
                p.d6 = Convert.ToInt32(txtD6.Text);
                p.fecha_sorteo = Convert.ToDateTime(dtpFechaSorteo.Text);
                p.joker = Convert.ToInt32(txtJoker.Text);

                using (Model.Nanpa_Kama_Model context = new Model.Nanpa_Kama_Model())
                {
                    data.lstCombinacionesHistorico.Add(p);
                    context.t_combinaciones_historico.Add(p);
                    context.SaveChanges();
                }

                string text = "Se ha guardado el nuevo registro correctamente.";
                DialogResult dialogResult = MessageBox.Show(text, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                foreach (Control ctl in grbCombinacion.Controls)
                {
                    ui.LimpiarControl(ctl);
                }
            }
        }

        private void btnCargarHistorico_Click(object sender, EventArgs e)
        {
            txtBuscarD1.Text = string.Empty;
            txtBuscarD2.Text = string.Empty;
            txtBuscarD3.Text = string.Empty;
            txtBuscarD4.Text = string.Empty;
            txtBuscarD5.Text = string.Empty;
            txtBuscarD6.Text = string.Empty;

            CargarHistorico();
        }

        private void Buscar()
        {
            List<Model.t_combinaciones_historico> lstH = data.lstCombinacionesHistorico;

            for (int i = 1; i < 6; i++)
            {
                switch (i)
                {
                    case 1:
                        if (!string.IsNullOrEmpty(txtBuscarD1.Text))
                        {
                            lstH = lstH.Where(w => w.d1 == Convert.ToInt32(txtBuscarD1.Text)).Select(s => s).ToList();
                        }
                        break;

                    case 2:
                        if (!string.IsNullOrEmpty(txtBuscarD2.Text))
                        {
                            lstH = lstH.Where(w => w.d2 == Convert.ToInt32(txtBuscarD2.Text)).Select(s => s).ToList();
                        }
                        break;

                    case 3:
                        if (!string.IsNullOrEmpty(txtBuscarD3.Text))
                        {
                            lstH = lstH.Where(w => w.d3 == Convert.ToInt32(txtBuscarD3.Text)).Select(s => s).ToList();
                        }
                        break;

                    case 4:
                        if (!string.IsNullOrEmpty(txtBuscarD4.Text))
                        {
                            lstH = lstH.Where(w => w.d4 == Convert.ToInt32(txtBuscarD4.Text)).Select(s => s).ToList();
                        }
                        break;

                    case 5:
                        if (!string.IsNullOrEmpty(txtBuscarD5.Text))
                        {
                            lstH = lstH.Where(w => w.d5 == Convert.ToInt32(txtBuscarD5.Text)).Select(s => s).ToList();
                        }
                        break;

                    case 6:
                        if (!string.IsNullOrEmpty(txtBuscarD6.Text))
                        {
                            lstH = lstH.Where(w => w.d6 == Convert.ToInt32(txtBuscarD6.Text)).Select(s => s).ToList();
                        }
                        break;

                    default:
                        break;
                }
            }

            dgvDatos.DataSource = util.ToDataTable(lstH);
        }
    }
}
